package oop2.tp3.ejercicio1;

public class Libro {
    public static final int MONTO_BASE = 2;
    private String nombre;

    public Libro(String nombre) {
        this.nombre = nombre;
    }

    public String nombre() {
        return nombre;
    }

    public double calcularCostoPorDias(int diasAlquilado) {
        double monto = MONTO_BASE;
        if (diasAlquilado > 2) {
            monto += (diasAlquilado - 2) * 1.5;
        }
        return monto;
    }

    public boolean puntoBonus(int diasAlquilado) {
        return false;
    }
}
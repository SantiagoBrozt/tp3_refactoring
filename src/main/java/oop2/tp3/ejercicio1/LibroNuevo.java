package oop2.tp3.ejercicio1;

public class LibroNuevo extends Libro{

    public LibroNuevo(String nombre) {
        super(nombre);
    }

    @Override
    public double calcularCostoPorDias(int diasAlquilado) {
        return diasAlquilado * 3;
    }

    @Override
    public boolean puntoBonus(int diasAlquilado) {
        return diasAlquilado > 1;
    }
}

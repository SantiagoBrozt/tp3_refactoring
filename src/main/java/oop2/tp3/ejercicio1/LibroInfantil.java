package oop2.tp3.ejercicio1;

public class LibroInfantil extends Libro{

    public static final double MONTO_BASE = 1.5;

    public LibroInfantil(String nombre) {
        super(nombre);
    }

    @Override
    public double calcularCostoPorDias(int diasAlquilado) {
        double monto = MONTO_BASE;
        if (diasAlquilado > 3) {
            monto += (diasAlquilado - 3) * 1.5;
        }
        return monto;
    }

}

package oop2.tp3.ejercicio3;

public class Cena extends Gasto {

    public Cena(int monto) {
        super(monto);
    }

    @Override
    public int sumarMontoComida(int montoTotal) {
        return montoTotal += monto();
    }

    @Override
    public boolean esExceso() {
        return monto() > 5000;
    }

    @Override
    public String tipoDeGasto() {
        return "Cena";
    }
}

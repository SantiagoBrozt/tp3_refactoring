package oop2.tp3.ejercicio3;

import java.time.LocalDate;
import java.util.List;
import java.util.function.Supplier;

public class EjemploDeUsoDelReporte {
    public static void main(String[] args) {
        var g1 = new Desayuno(1000);
        var reporte = new ReporteDeGastos(new Supplier<LocalDate>() {
            @Override
            public LocalDate get() {
                return LocalDate.now();
            }
        }, g1);
        System.out.println(reporte.imprimir());
    }
}

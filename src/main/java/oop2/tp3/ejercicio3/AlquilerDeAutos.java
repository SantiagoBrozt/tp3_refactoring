package oop2.tp3.ejercicio3;

public class AlquilerDeAutos extends Gasto{

    public AlquilerDeAutos(int monto) {
        super(monto);
    }

    @Override
    public String tipoDeGasto() {
        return "Alquiler de autos";
    }
}

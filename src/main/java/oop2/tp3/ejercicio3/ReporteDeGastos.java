package oop2.tp3.ejercicio3;

import java.time.LocalDate;
import java.util.List;
import java.util.function.Supplier;

public class ReporteDeGastos {
    private List<Gasto> gastos;
    private Supplier<LocalDate> proveedorDeFecha;

    public ReporteDeGastos(Supplier<LocalDate> proveedorDeFecha, Gasto... gastos) {
        this.gastos = List.of(gastos);
        this.proveedorDeFecha = proveedorDeFecha;
    }
    public String imprimir() {
        int total = 0;
        int gastosDeComida = 0;
        String reporte = "Expenses " + proveedorDeFecha.get() + "\n";

        for (Gasto gasto : gastos) {
            gastosDeComida = gasto.sumarMontoComida(gastosDeComida);

            reporte += gasto.tipoDeGasto() + "\t" + gasto.monto() + "\t" + (gasto.esExceso() ? "X" : " ") + "\n";

            total += gasto.sumarMontoTotal(total);
        }
        return reporte += "Gastos de comida: " + gastosDeComida + "\n" + "Total de gastos: " + total;
    }
}

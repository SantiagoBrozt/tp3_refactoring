package oop2.tp3.ejercicio3;

public class Desayuno extends Gasto {

    public Desayuno(int monto) {
        super(monto);
    }

    @Override
    public int sumarMontoComida(int montoTotal) {
        return montoTotal += monto();
    }

    @Override
    public boolean esExceso() {
        return monto() > 1000;
    }

    @Override
    public String tipoDeGasto() {
        return "Desayuno";
    }
}

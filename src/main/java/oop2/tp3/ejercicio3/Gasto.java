package oop2.tp3.ejercicio3;

public class Gasto {
        private int monto;

        public Gasto(int monto) {
                this.monto = monto;
        }
        public int monto() {
                return monto;
        }

        public String tipoDeGasto() {
                return "Gasto";
        }

        public boolean esExceso() {
                return false;
        }

        public int sumarMontoTotal(int montoTotal) {
                return montoTotal += monto;
        }

        public int sumarMontoComida(int montoTotal) {
                return montoTotal;
        }
}



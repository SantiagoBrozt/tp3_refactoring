package oop2.tp3.ejercicio2;

import com.opencsv.CSVReader;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LectorCSV {
    private String localPath;

    public LectorCSV(String path) throws IOException {
        this.localPath = path;
    }
    public List<String[]> leer() throws IOException {
        List<String[]> datos = new ArrayList<String[]>();
        CSVReader reader = new CSVReader(new FileReader(localPath));
        String[] row = null;

        while ((row = reader.readNext()) != null) {
            datos.add(row);
        }
        reader.close();
        datos.remove(0);

        return datos;
    }
}

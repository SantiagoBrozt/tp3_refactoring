package oop2.tp3.ejercicio1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AlquilerTest {
    @Test
    public void alquiler() {
        Libro elTunel = new Libro("El Túnel");
        Libro antesDelFin = new LibroNuevo("Antes del Fin");
        Libro elPrincipito = new LibroInfantil("El Principito");
        CopiaLibro elTunelCopia = new CopiaLibro(elTunel);
        CopiaLibro antesDelFinCopia = new CopiaLibro(antesDelFin);
        CopiaLibro elPrincipitoCopia = new CopiaLibro(elPrincipito);
        Alquiler alquilerElTunel = new Alquiler(elTunelCopia, 5);
        Alquiler alquilerAntesDelFin = new Alquiler(antesDelFinCopia, 3);
        Alquiler alquilerElPrincipito = new Alquiler(elPrincipitoCopia, 4);
        Cliente yo = new Cliente("Javier");
        yo.alquilar(alquilerElTunel);
        yo.alquilar(alquilerAntesDelFin);
        yo.alquilar(alquilerElPrincipito);
        Object[] resultado = yo.calcularDeudaYPuntosObtenidos();
        assertEquals(18.5, resultado[0]);
        assertEquals(4 , resultado[1]);
    }
}

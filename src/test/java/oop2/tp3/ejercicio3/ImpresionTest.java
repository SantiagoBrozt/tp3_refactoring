package oop2.tp3.ejercicio3;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ImpresionTest {
    @Test
    public void impresion() {
        var g1 = new Desayuno(1000);
        var reporte = new ReporteDeGastos(new FakeProveedorDeFecha(), g1);

        assertEquals(
                "Expenses 2024-04-19\n" +
                "Desayuno\t1000\t \n" +
                "Gastos de comida: 1000\n" +
                "Total de gastos: 1000", reporte.imprimir());
    }
}
